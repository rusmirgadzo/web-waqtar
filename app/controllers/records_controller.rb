class RecordsController < ApplicationController

  def index
    @tasks = Task.all
  end

  def new
    # Praktično nova instanca metode new
    @record = Record.new
    # Naredna linija koda čini mi dostupnim Task model radi čitanja ID-a
    @tasks = Task.all
  end

  def create
    @record = Record.new(recordparams)

    if @record.save
      redirect_to root_path()
    else
      render :new
    end
  end

  def show
    record_id = params[:id]
    @record = Record.find(record_id)
    @task = Task.where(id: params[:id])
  end

  def recordparams
    params.require(:record).permit(:begintime, :endtime, :difftime, :taskid, :recorddate)
  end


end
