class TasksController < ApplicationController

  def new
    @task = Task.new
  end

  def create
    @task = Task.new(taskparams)

    if @task.save
      redirect_to root_path()
    else
      render :new
    end
  end

  def show
    task_id = params[:id]
    @task = Task.find(task_id)
    @records = Record.all.where(taskid: params[:id])
  end

  def edit
    @task = Task.find(params[:id])
  end

  def update
    @task = Task.find(params[:id])
    task_name = @task.taskname
    if @task.update_attributes(taskparams)
      flash[:notice] = "Zadatak pod naslovom -| #{task_name} |- ažuriran!"
      redirect_to root_path()
    else
      render :edit
    end

  end

  def destroy
    @task = Task.find(params[:id])
    task_name = @task.taskname
    @task.destroy()
    flash[:notice] = "Zadatak pod naslovom -| #{task_name} |- obrisan!"
    redirect_to root_path()
  end



  def taskparams
    params.require(:task).permit(:taskname, :clientname, :taskstatus, :taskdesc)
  end

end
