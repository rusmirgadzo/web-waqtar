class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.time :begintime
      t.time :endtime
      t.integer :difftime
      t.integer :taskid
      t.datetime :recorddate

      t.timestamps null: false
    end
  end
end
