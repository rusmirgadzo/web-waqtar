class ChangeFieldTypeRecords < ActiveRecord::Migration
  def change
    change_column :records, :begintime, :string
    change_column :records, :endtime, :string
  end
end
