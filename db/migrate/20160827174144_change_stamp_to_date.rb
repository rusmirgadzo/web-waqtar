class ChangeStampToDate < ActiveRecord::Migration
  def change
    change_column :records, :recorddate, :date
  end
end
